Asi que quieres entrenar una red neuronal usando Keras  
primero unas consideraciones:  

Se asume que a lo menos se hizo una revision por encima de algunos ejemplos de  
keras, por lo menos los más basicos  
https://github.com/keras-team/keras/tree/master/examples  

# Problemas de formato de tus datos

Keras en el caso de imagenes requiere que tus datos tengan las dimensiones  
```
(CantidadDeDatos, x, y, channels)
``` 
Si tus imagenes tienen la forma (x, y) por ejemplo siendo una imagen en blanco y  
negro, la forma de unirlas seria  
```
out = np.concatenate([arr[None,..., None] for arr in data], axis=0)  
```
siendo data tu arreglo con imagenes con dimensiones (x,y)  
de este modo tu nuevo arreglo tendria la forma  
```
(CantidadDeDatos,x,y,1), de forma similar puede hacerse para imagenes con 3 canales
```



# Posibles problemas con Numpy

## Tener tus arreglos de numpy en una lista y convertirlos a un solo arreglo de numpy no parece ser una opcion

Pueden haber varios problemas aqui asociados, pasaremos uno a uno  

Dado que las listas de python funcionan mucho más rapido que la concatenacion de  
numpy, es posible que tengas tus datos agrupados en una lista, ahora el problema  
es que keras no acepta listas como entradas, en ese caso tienes la siguiente  
opcion:  

Siendo datain tu lista con tus arreglos

```
np.array(datain)
```  


Lo convierte facilmente a un arreglo numpy, listo para ser ingresado a keras.

```
modelo.fit(datain, dataout,...)
```

En caso de que el computador se "cuelgue", pues otras medidas seran necesarias, 
especificamente funciones generadoras en python, no confundir con aumentacion de  
datos.

Una funcion generadora es similar a una funcion normal en python, solo que esta  
definida para ejecutarse de forma iterativa infinitamente (?), y en lugar de  
returns, tiene yields que son como returns, pero para generadores, puedes leer  
más al respecto aqui https://wiki.python.org/moin/Generators
Basicamente tiene 3 partes, un while que puede ser infinito, el procesamiento  
que desees hacer y el yield.

```
def firstn():
    num = 0
    while num < n:
    yield num
    num += 1
```
entonces para entrenar tu red neuronal
```
added.fit_generator(getarray(), epochs=epochs, steps_per_epoch=steps)
```  
esto solo un ejemplo, pero se entiende la idea, lo importante es que la salida de  
la funcion generadora concuerde con lo que necesite la red neuronal, en caso de  
una red con solo una salida y una entrada, pues un (x,y) estaria bien, en otros  
complejos adaptar.

todo:
· terminar